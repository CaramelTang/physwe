//全局
var filename='';
var filepath='';

//编辑器设置
KindEditor.options.filterMode = false;
var options = {    cssPath : '../kindeditor/themes/default/default.css'};

KindEditor.ready(function(K) {
    window.editor = K.create('#editor_id',{
	uploadJson : '../kindeditor/php/upload_json.php',
	fileManagerJson : '../kindeditor/php/file_manager_json.php',
	allowFileManager : true	
    });
});			   

//上传文件
    jQuery(function(){   
	$("#buttonUpload").click(function(){     
            //加载图标   
            $("#loading").ajaxStart(function(){
               $(this).show();
               }).ajaxComplete(function(){
               $(this).hide();
            });
            //上传文件
            $.ajaxFileUpload({
		url:'upload.php',
		secureuri :false,
		fileElementId :'fileToUpload',//file控件id
		dataType : 'json',
		success : function (data, status){
                    if(typeof(data.error) != 'undefined'){
			if(data.error != ''){
                            alert(data.error);
			}else{
                            alert(data.msg);
			    //记录上传获得的信息
			    filename=data.filename;
			    filepath=data.filepath;
			    //已上传附件显示
			    document.getElementById("filename").innerHTML='已上传的附件:'+filename;			
			    $("#filename").show();
			}
                    }
		},
		error: function(data, status, e){
                    //alert(e);
		    alert('上传失败，请与管理员联系。');
		}
	    })
	    return false;
	}) 
    })

//发布通知
function check()
{

    editor.sync();//获取编辑器内容之前 要同步
    var content= document.getElementById('editor_id').value; // 原生API

    var title=document.getElementById("title").value;
    //var content =document.getElementById("content").value;
    var tag=document.getElementById("tag").value;
    //var who=document.getElementById("who").value;
    var grade=document.getElementById("grade").value;
    var xmlhttp;
    if (title==''||content==''||tag==''||grade=='-2')
    {
	alert('标题、正文、标签、面向对象不允许为空。');
	return ;
    }    
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
	    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
    xmlhttp.onreadystatechange=function()
    {
	if (xmlhttp.readyState==4 && xmlhttp.status==200)
	{
	    if(xmlhttp.responseText=='success')
	    {
		alert('发布成功。');
		self.location='../home.php'; 
	    }
	    else{
		alert('发布失败，请与管理员联系。');
		//alert(xmlhttp.responseText);
	    }
	}
    }


    //先讲%&+转义，再做URI编码
    title = title.replace(/%/g, "%25");
    title = title.replace(/\&/g, "%26");
    title = title.replace(/\+/g, "%2B");
    content = content.replace(/%/g, "%25");
    content = content.replace(/\&/g, "%26");
    content = content.replace(/\+/g, "%2B");
    tag = tag.replace(/%/g, "%25");
    tag = tag.replace(/\&/g, "%26");
    tag = tag.replace(/\+/g, "%2B");
    filename = filename.replace(/%/g, "%25");
    filename = filename.replace(/\&/g, "%26");
    filename = filename.replace(/\+/g, "%2B");
    filepath = filepath.replace(/%/g, "%25");
    filepath = filepath.replace(/\&/g, "%26");
    filepath = filepath.replace(/\+/g, "%2B");


    title=encodeURI(title);
    content=encodeURI(content);
    tag=encodeURI(tag);
    filename=encodeURI(filename);
    filepath=encodeURI(filepath);
    
    xmlhttp.open("POST","admin.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("title="+title+"&content="+content+"&tag="+tag+"&grade="+grade+"&filename="+filename+"&filepath="+filepath);
}

//删除通知
function delnotice()
{
    var delnid=document.getElementById("delnid").value;
    var xmlhttp;
    if (delnid=='')
    {
	alert('请输入要删除的ID。');
	return ;
    }    
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
	    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
    xmlhttp.onreadystatechange=function()
    {
	if (xmlhttp.readyState==4 && xmlhttp.status==200)
	{
	    if(xmlhttp.responseText=='success')
	    {
		alert('删除成功。');
		self.location='admin.php'; 
	    }
	    else{
		alert('删除失败，请与管理员联系。');
		//alert(xmlhttp.responseText);
	    }
	}
    }
    xmlhttp.open("POST","admin.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send('delnid='+delnid);
}

//清除附件
function delfile()
{
    filename='';
    filepath='';
    //$("#filename").innerHTML='已上传的附件:  无';
    $("#filename").hide();
}

